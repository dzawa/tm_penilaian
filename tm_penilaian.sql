-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 17 Agu 2018 pada 15.10
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tm_penilaian`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_guru`
--

CREATE TABLE `tm_guru` (
  `id_guru` int(10) NOT NULL,
  `nip` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `nama_depan` text NOT NULL,
  `nama_belakang` text NOT NULL,
  `no_telp` varchar(55) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `status` set('Aktif','Tidak Aktif','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_jurusan`
--

CREATE TABLE `tm_jurusan` (
  `id_jurusan` int(10) NOT NULL,
  `nama_jurusan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_kelas`
--

CREATE TABLE `tm_kelas` (
  `id_kelas` int(10) NOT NULL,
  `id_jurusan` int(10) NOT NULL,
  `id_tingkat` int(10) NOT NULL,
  `id_seri` int(10) NOT NULL,
  `id_tahun_ajaran` int(10) NOT NULL,
  `id_kurikulum` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_komponen`
--

CREATE TABLE `tm_komponen` (
  `id_komponen` int(10) NOT NULL,
  `nama_komponen` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_kurikulum`
--

CREATE TABLE `tm_kurikulum` (
  `id_kurikulum` int(10) NOT NULL,
  `nama_kurikulum` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_mapel`
--

CREATE TABLE `tm_mapel` (
  `id_mapel` int(10) NOT NULL,
  `nama_mapel` text NOT NULL,
  `sks` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_matpel_guru_kelas`
--

CREATE TABLE `tm_matpel_guru_kelas` (
  `id_kelas` int(10) NOT NULL,
  `id_matpel` int(10) NOT NULL,
  `id_guru` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_penilaian`
--

CREATE TABLE `tm_penilaian` (
  `nis_siswa` int(10) NOT NULL,
  `id_semester_berjalan` int(10) NOT NULL,
  `id_komponen` int(10) NOT NULL,
  `nilai` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_semester`
--

CREATE TABLE `tm_semester` (
  `id_semester` int(10) NOT NULL,
  `index_semester` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_semester_berjalan`
--

CREATE TABLE `tm_semester_berjalan` (
  `id_semester_berjalan` int(10) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  `id_guru` int(10) NOT NULL,
  `id_semester` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_seri`
--

CREATE TABLE `tm_seri` (
  `id_seri` int(10) NOT NULL,
  `nama_seri` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_siswa`
--

CREATE TABLE `tm_siswa` (
  `nis` int(10) NOT NULL,
  `nama_depan` text NOT NULL,
  `nama_belakang` text NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `tgl_lahir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_siswa_kelas`
--

CREATE TABLE `tm_siswa_kelas` (
  `id_kelas` int(10) NOT NULL,
  `id_siswa` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_tahun_ajaran`
--

CREATE TABLE `tm_tahun_ajaran` (
  `id_tahun_ajaran` int(10) NOT NULL,
  `tahun_ajaran` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_tingkat`
--

CREATE TABLE `tm_tingkat` (
  `id_tingkat` int(10) NOT NULL,
  `tingkat_kelas` varchar(44) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_user`
--

CREATE TABLE `tm_user` (
  `id_user` int(10) NOT NULL,
  `email` varchar(55) NOT NULL,
  `password` varchar(55) NOT NULL,
  `level` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tm_guru`
--
ALTER TABLE `tm_guru`
  ADD PRIMARY KEY (`id_guru`),
  ADD KEY `nip` (`nip`);

--
-- Indexes for table `tm_jurusan`
--
ALTER TABLE `tm_jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `tm_kelas`
--
ALTER TABLE `tm_kelas`
  ADD PRIMARY KEY (`id_kelas`,`id_jurusan`,`id_tingkat`,`id_seri`,`id_tahun_ajaran`,`id_kurikulum`);

--
-- Indexes for table `tm_komponen`
--
ALTER TABLE `tm_komponen`
  ADD PRIMARY KEY (`id_komponen`);

--
-- Indexes for table `tm_kurikulum`
--
ALTER TABLE `tm_kurikulum`
  ADD PRIMARY KEY (`id_kurikulum`);

--
-- Indexes for table `tm_mapel`
--
ALTER TABLE `tm_mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `tm_matpel_guru_kelas`
--
ALTER TABLE `tm_matpel_guru_kelas`
  ADD PRIMARY KEY (`id_kelas`,`id_matpel`,`id_guru`);

--
-- Indexes for table `tm_penilaian`
--
ALTER TABLE `tm_penilaian`
  ADD PRIMARY KEY (`nis_siswa`,`id_semester_berjalan`,`id_komponen`);

--
-- Indexes for table `tm_semester`
--
ALTER TABLE `tm_semester`
  ADD PRIMARY KEY (`id_semester`);

--
-- Indexes for table `tm_semester_berjalan`
--
ALTER TABLE `tm_semester_berjalan`
  ADD PRIMARY KEY (`id_semester_berjalan`,`id_mapel`,`id_kelas`,`id_guru`,`id_semester`);

--
-- Indexes for table `tm_seri`
--
ALTER TABLE `tm_seri`
  ADD PRIMARY KEY (`id_seri`);

--
-- Indexes for table `tm_siswa`
--
ALTER TABLE `tm_siswa`
  ADD PRIMARY KEY (`nis`);

--
-- Indexes for table `tm_siswa_kelas`
--
ALTER TABLE `tm_siswa_kelas`
  ADD PRIMARY KEY (`id_kelas`,`id_siswa`);

--
-- Indexes for table `tm_tahun_ajaran`
--
ALTER TABLE `tm_tahun_ajaran`
  ADD PRIMARY KEY (`id_tahun_ajaran`);

--
-- Indexes for table `tm_tingkat`
--
ALTER TABLE `tm_tingkat`
  ADD PRIMARY KEY (`id_tingkat`);

--
-- Indexes for table `tm_user`
--
ALTER TABLE `tm_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tm_guru`
--
ALTER TABLE `tm_guru`
  MODIFY `id_guru` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tm_jurusan`
--
ALTER TABLE `tm_jurusan`
  MODIFY `id_jurusan` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tm_mapel`
--
ALTER TABLE `tm_mapel`
  MODIFY `id_mapel` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tm_semester`
--
ALTER TABLE `tm_semester`
  MODIFY `id_semester` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tm_seri`
--
ALTER TABLE `tm_seri`
  MODIFY `id_seri` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tm_tahun_ajaran`
--
ALTER TABLE `tm_tahun_ajaran`
  MODIFY `id_tahun_ajaran` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tm_tingkat`
--
ALTER TABLE `tm_tingkat`
  MODIFY `id_tingkat` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tm_user`
--
ALTER TABLE `tm_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
