<!DOCTYPE html>
<html lang="en">
<head>
	<title>
		<?php echo $__env->yieldContent('title'); ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <?php echo $__env->make('layouts.css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->yieldContent('css'); ?>
    <style type="text/css">
		body {
		  background: #eecfff;
		  -webkit-font-smoothing: antialiased;
		}
    </style>
</head>
<body>
<!-- HEADER -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 header">
				<span class="logo-header">
					<img src="asset/image/logo-header.png">
				</span>
			</div>
<!-- MAIN-KONTEN -->
			<?php echo $__env->yieldContent('content'); ?>

    </div>
  </div>
<!-- Script -->
      <?php echo $__env->make('layouts.js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldContent('script'); ?>
</body>
</html>
