<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginApiTest extends TestCase
{
    use MakeLoginTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateLogin()
    {
        $login = $this->fakeLoginData();
        $this->json('POST', '/api/v1/logins', $login);

        $this->assertApiResponse($login);
    }

    /**
     * @test
     */
    public function testReadLogin()
    {
        $login = $this->makeLogin();
        $this->json('GET', '/api/v1/logins/'.$login->id);

        $this->assertApiResponse($login->toArray());
    }

    /**
     * @test
     */
    public function testUpdateLogin()
    {
        $login = $this->makeLogin();
        $editedLogin = $this->fakeLoginData();

        $this->json('PUT', '/api/v1/logins/'.$login->id, $editedLogin);

        $this->assertApiResponse($editedLogin);
    }

    /**
     * @test
     */
    public function testDeleteLogin()
    {
        $login = $this->makeLogin();
        $this->json('DELETE', '/api/v1/logins/'.$login->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/logins/'.$login->id);

        $this->assertResponseStatus(404);
    }
}
