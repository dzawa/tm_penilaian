<?php

use Faker\Factory as Faker;
use App\Models\Login;
use App\Repositories\LoginRepository;

trait MakeLoginTrait
{
    /**
     * Create fake instance of Login and save it in database
     *
     * @param array $loginFields
     * @return Login
     */
    public function makeLogin($loginFields = [])
    {
        /** @var LoginRepository $loginRepo */
        $loginRepo = App::make(LoginRepository::class);
        $theme = $this->fakeLoginData($loginFields);
        return $loginRepo->create($theme);
    }

    /**
     * Get fake instance of Login
     *
     * @param array $loginFields
     * @return Login
     */
    public function fakeLogin($loginFields = [])
    {
        return new Login($this->fakeLoginData($loginFields));
    }

    /**
     * Get fake data of Login
     *
     * @param array $postFields
     * @return array
     */
    public function fakeLoginData($loginFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'password' => $fake->word,
            'level' => $fake->text
        ], $loginFields);
    }
}
