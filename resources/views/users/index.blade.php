@extends('layouts.app')
@section('css')
<style type="text/css">
  body {
    margin: 0px;
    padding: 0px;
  }
</style>
@endsection
@section('title', 'Index')
@section('dropdown-right')
@include('layouts.dropdown-right')
@endsection
@section('content')
<!-- bredcrum -->
<div class="row">
  <div class="col-12" style="padding: 0;">
    <div style="width: 100%;">
      <ol class="breadcrumb breadcrumb-arrow">
        <li class="active"><span>Home</span></li>
      </ol>
    </div>
  </div>
</div>
<!-- MAIN KONTEN -->
<div class="row main-konten">
  <div class='col-md-12 flex-inline'>
    <!-- SELECT * FROM tm_kelas INNER JOIN tm_tahun_ajaran ON tm_kelas.id_tahun_ajaran = tm_tahun_ajaran.id_tahun_ajaran INNER JOIN tm_guru ON tm_kelas.nik = tm_guru.nik WHERE tm_kelas.nik=22 -->
    <div class='col-md-4'>
      <a href="tahun-ajaran.php">
        <div class="thumb">
          <header style="text-decoration: none;"><i class="fa fa-cogs" aria-hidden="true"></i>
            <h3 class="thumb-judul" style="text-decoration: none;">Kelola Nilai</h3> </header>
          <article>
            <p class="thumb-text">Pengelolaan / Pemasukan Nilai.</p>
          </article>
        </div>
      </a>
    </div>
  </div>
</div>
</div>
<div class="clear"></div>
<div class="footer">
  Copyright&copy; SMK Tunas Media. Created by Danang Widianto
</div>
@endsection
