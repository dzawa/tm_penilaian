@extends('layouts.app')
@section('title','Admin')
@section('content')
  <!-- MAIN-KONTEN -->
      <div class="row">
        <!-- =========================== RIGHT KONTEN ================ -->
      @include('layouts.right-konten-admin')
        <!-- =============================== LEFT KONTEN ===================== -->
              <div class="col" style="padding: 0;">
                <div class="row">
                  <div class="col-md-12">
                      <ol class="breadcrumb breadcrumb-arrow">
                          <li class="active"><span>Home</span></li>
                      </ol>
                  </div>
                </div>
                <div class="row">
                  <div class='col-md-4'>
                     <a href="admin_guru.php">
                      <div class="thumb">
                        <header style="text-decoration: none;"><i class="fa fa-id-badge" aria-hidden="true"></i>
                            <h3 class="thumb-judul" style="text-decoration: none;">Data Guru</h3> </header>
                        <article>
                            <!-- <p class="thumb-text">Pengelolaan / Pemasukan Nilai.</p> -->
                        </article>
                      </div>
                    </a>
                  </div>
                  <div class='col-md-4'>
                     <a href="admin_siswa.php">
                      <div class="thumb">
                        <header style="text-decoration: none;"><i class="fa fa-id-card" aria-hidden="true"></i>
                            <h3 class="thumb-judul" style="text-decoration: none;">Data Siswa</h3> </header>
                        <article>
                            <!-- <p class="thumb-text">Pengelolaan / Pemasukan Nilai.</p> -->
                        </article>
                      </div>
                    </a>
                  </div>
                  <div class='col-md-4'>
                     <a href="admin_jurusan.php">
                      <div class="thumb">
                        <header style="text-decoration: none;"><i class="fa fa-sitemap" aria-hidden="true"></i>
                            <h3 class="thumb-judul" style="text-decoration: none;">Jurusan</h3> </header>
                        <article>
                            <!-- <p class="thumb-text">Pengelolaan / Pemasukan Nilai.</p> -->
                        </article>
                      </div>
                    </a>
                  </div>
                  <div class='col-md-4'>
                     <a href="kelas.php">
                      <div class="thumb">
                        <header style="text-decoration: none;"><i class="fa fa-users" aria-hidden="true"></i>
                            <h3 class="thumb-judul" style="text-decoration: none;">Kelas</h3> </header>
                        <article>
                            <!-- <p class="thumb-text">Pengelolaan / Pemasukan Nilai.</p> -->
                        </article>
                      </div>
                    </a>
                  </div>
                  <div class='col-md-4'>
                     <a href="admin_mapel.php">
                      <div class="thumb">
                        <header style="text-decoration: none;"><i class="fa fa-book" aria-hidden="true"></i>
                            <h3 class="thumb-judul" style="text-decoration: none;">Mata Pelajaran</h3> </header>
                        <article>
                            <!-- <p class="thumb-text">Pengelolaan / Pemasukan Nilai.</p> -->
                        </article>
                      </div>
                    </a>
                  </div>
                  <div class='col-md-4'>
                     <a href="admin_tahun-ajaran.php">
                      <div class="thumb">
                        <header style="text-decoration: none;"><i class="fa fa-calendar-alt" aria-hidden="true"></i>
                            <h3 class="thumb-judul" style="text-decoration: none;">Tahun Ajaran</h3> </header>
                        <article>
                            <!-- <p class="thumb-text">Pengelolaan / Pemasukan Nilai.</p> -->
                        </article>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
      
          <div class="clear"></div>
          <div class="footer">
              Copyright&copy; SMK Tunas Media. Created by Danang Widianto
          </div>
@endsection
