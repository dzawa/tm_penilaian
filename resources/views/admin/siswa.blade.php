@extends('layouts.admin')
@section('content')

<div class="col" style="padding: 0;">
  <div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb breadcrumb-arrow">
          <li><a href="admin_index.php">Home</a></li>
            <li class="active"><span>Data Siswa</span></li>
        </ol>
    </div>
  </div>
  <div class="col-lg-12">
    <div class="judul-konten" style="text-align: center; font-size: 20px;">
      <h3>Data Siswa</h3>
    </div>
     <div class="form-tambah">
      <h3 class="judul-form">Form Siswa</h3>
        <table border="0">
          <form action="../php/p_input_siswa.php" class="form-data">
            <tr class="tr-border">
              <td style="width: 150px"><label>NIP</label></td>
              <td><input type="text" class="form-control form-nama" name="nip"></td>
            </tr>
            <tr class="tr-border">
              <td><label>NIS</label></td>
              <td><input type="text" class="form-control form-nama" style="text-transform: uppercase;" name="nis"></td>
            </tr>
            <tr class="tr-border">
              <td><label>Nama Siswa</label></td>
              <td><input type="text" class="form-control form-nama" name="nama"></td>
            </tr>
            <tr class="tr-border">
              <td><label>Jurusan</label></td>
              <td>
                <select class="form-control" name="jurusan">
                  <?php
                      $query_jurusan = mysql_query("SELECT * from tm_jurusan");
                      while ($array_jurusan = mysql_fetch_array($query_jurusan)) {
                        ?>
                        <option value="<?php echo $array_jurusan['nama_jurusan'] ?>"><?php echo $array_jurusan['nama_jurusan'] ?></option>
                        <?php
                      }
                  ?>
                </select>
              </td>
            </tr>
            <tr class="tr-border">
              <td><label>Kelas</label></td>
              <td>
                <select class="form-control" name="kelas">
                  <?php
                      $query_jurusan = mysql_query("SELECT * from tm_kelas");
                      while ($array_jurusan = mysql_fetch_array($query_jurusan)) {
                        ?>
                        <option value="<?php echo $array_jurusan['kelas'] ?>"><?php echo $array_jurusan['kelas'] ?></option>
                        <?php
                      }
                  ?>
                </select>
              </td>
            </tr>
            <tr>
              <td></td>
              <td><button data-toggle="modal" data-target="#modalguru" class="btn-tambah btn btn-primary pointer"><i class="fa fa-user-plus" aria-hidden="true"></i> Tambah Siswa</button></td>
            </tr>
        </form>
        </table>
    </div>
<!-- ======================================= SEARCH ================== -->
    <h3 class="judul-form" style="margin-bottom: 10px;">Table Guru</h3>
    <div class="row">
      <div class="col-lg-3" style="margin-bottom: 5px;">
        <form action="" method="get">
          <div class="input-group" style="width: 100%;">
            <input type="text" name="search" class="form-control field-search" placeholder="Nama" aria-describedby="search" value="<?php if(isset($_GET['search'])){ echo $_GET['search'];} ?>">
            <button type="submit" class="input-group-addon btn-search pointer btn" id="search"><i class="fa fa-search" aria-hidden="true"></i></button>
            <a href="u_uh.php" class="input-group-addon btn-refresh pointer btn btn-success"><i class="fa fa-sync-alt"></i></a>
          </div>
        </form>
       </div>
       <div class="col-lg-9">
            <a href="#" class="btn btn-primary" style="float: right;">Print <i class="fa fa-print"></i></a>
       </div>
    </div>
<!-- =================== TABLE ================ -->
    <div class="scroll-table">
      <table class="table table-shadow table-hover table-responsive">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="15%">NIS</th>
            <th width="50%">Nama</th>
            <th width="10%">id jurusan</th>
            <th width="10%">id kelas</th>
            <th width="10%">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $query = mysql_query("select * from tm_siswa");
          $no = 1;
          while ($field = mysql_fetch_array($query)) {
            ?>
          <tr>
            <form method="post" action="../php/p_input_uh.php">
              <td><?php echo $no ?></td>
              <td><?php echo $field['nis'] ?></td>
              <td><?php echo $field['nama_siswa'] ?></td>
                <!-- <input type="hidden" name="nis" maxlength="3" size="2" value=<?php echo $field['nis'] ?>> -->
              <td><?php echo $field['id_jurusan'] ?></td>
              <td><?php echo $field['id_kelas'] ?></td>
              <td style="text-align: center;"><i class="edit-cel fa fa-pencil-square-o pointer" aria-hidden="true"></i></td>
            </form>
          </tr>
            <?php
            $no++;
          }
          ?>
          </tbody>
      </table>
    </div>
  </div>
</div>

@endsection
