@extends('layouts.admin')
@section('content')

  <div class="col" style="padding: 0;">
    <div class="row">
      <div class="col-md-12">
          <ol class="breadcrumb breadcrumb-arrow">
            <li><a href="#">Home</a></li>
              <li class="active"><span>Data Siswa</span></li>
          </ol>
      </div>
    </div>`
    <div class="row">
      <div class="col-md-12">
        <button data-toggle="modal" data-target="#modalguru" class="btn btn-primary" style="margin: 20px;"><i class="fa fa-user-plus" aria-hidden="true"></i> Tambah Siswa</button>
        <div class="scroll-table" style="margin: 0 20px">
          <table class="table table-shadow table-hover table-responsive">
            <thead>
              <tr>
                <th width="5%">No</th>
                <th width="15%">ID Kelas</th>
                <th width="50%">Kelas</th>
                <th width="10%">id jurusan</th>
                <th width="10%">id kelas</th>
                <th width="10%">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              include "../php/koneksi.php";
              $query = mysql_query("select * from tm_siswa");
              $no = 1;
              while ($field = mysql_fetch_array($query)) {
                ?>
              <tr>
                <form method="post" action="../php/p_input_uh.php">
                  <td><?php echo $no ?></td>
                  <td><?php echo $field['nis'] ?></td>
                  <td><?php echo $field['nama_siswa'] ?></td>
                    <!-- <input type="hidden" name="nis" maxlength="3" size="2" value=<?php echo $field['nis'] ?>> -->
                  <td><?php echo $field['id_jurusan'] ?></td>
                  <td><?php echo $field['id_kelas'] ?></td>
                  <td style="text-align: center;"><i class="edit-cel fa fa-pencil-square-o pointer" aria-hidden="true"></i></td>
                </form>
              </tr>
                <?php
                $no++;
              }
              ?>
              </tbody>
          </table>
          <div class="modal fade" id="modalguru" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="form-group">
                      <label>NIS</label>
                    <input type="input" class="form-control form-nama" name="nis">
                    </div>
                    <div class="form-group">
                      <label>Nama</label>
                    <input type="input" class="form-control form-nama" name="nama">
                    </div>
                    <div class="form-group">
                      <label>ID Jurusan</label>
                    <input type="input" class="form-control form-nama" name="id_jurusan">
                    </div>
                    <div class="form-group">
                      <label>ID Kelas</label>
                    <input type="input" class="form-control form-nama" name="nik">
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
