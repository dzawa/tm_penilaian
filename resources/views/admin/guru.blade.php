@extends('layouts.admin')
@section('content')

  <div class="col" style="padding: 0;">
    <div class="row">
      <div class="col-lg-12">
          <ol class="breadcrumb breadcrumb-arrow">
            <li><a href="admin_index.php">Home</a></li>
              <li class="active"><span>Data Guru</span></li>
          </ol>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="judul-konten" style="text-align: center; font-size: 20px;">
        <h3>Data Guru</h3>
      </div>
       <div class="form-tambah">
        <h3 class="judul-form">Form Guru</h3>
          <table border="0">
            <form action="../php/p_input_guru.php" class="form-data">
              <tr class="tr-border">
                <td style="width: 150px"><label>NIP</label></td>
                <td><input type="text" class="form-control form-nama" name="nip"></td>
              </tr>
              <tr class="tr-border">
                <td><label>Nama</label></td>
                <td><input type="text" class="form-control form-nama" style="text-transform: uppercase;" name="nama"></td>
              </tr>
              <tr class="tr-border">
                <td><label>Mapel</label></td>
                <td><input type="text" class="form-control form-nama" name="nama"></td>
              </tr>
              <tr class="tr-border">
                <td><label>level</label></td>
                <td><input type="text" class="form-control form-nama" name="nama"></td>
              </tr>
              <!-- <tr>
                 <td><label>status</label></td>
                <td><input type="text" class="form-control form-nama" name="nama"></td>
              </tr> -->
              <tr class="tr-border">
                <td><label>Tanggal Lahir</label></td>
                <td><input type="date" class="form-control form-nama" name="nama"></td>
              </tr>
              <tr>
                <td></td>
                <td><button data-toggle="modal" data-target="#modalguru" class="btn-tambah btn btn-primary pointer"><i class="fa fa-user-plus" aria-hidden="true"></i> Tambah Guru</button></td>
              </tr>
          </form>
          </table>
      </div>
<!-- ======================================= SEARCH ================== -->
      <h3 class="judul-form" style="margin-bottom: 10px;">Table Guru</h3>
      <div class="row">
        <div class="col-lg-3" style="margin-bottom: 5px;">
          <form action="" method="get">
            <div class="input-group" style="width: 100%;">
              <input type="text" name="search" class="form-control field-search" placeholder="Nama" aria-describedby="search" value="<?php if(isset($_GET['search'])){ echo $_GET['search'];} ?>">
              <button type="submit" class="input-group-addon btn-search pointer btn" id="search"><i class="fa fa-search" aria-hidden="true"></i></button>
              <a href="u_uh.php" class="input-group-addon btn-refresh pointer btn btn-success"><i class="fa fa-sync-alt"></i></a>
            </div>
          </form>
         </div>
         <div class="col-lg-9">
              <a href="#" class="btn btn-primary" style="float: right;">Print <i class="fa fa-print"></i></a>
         </div>
      </div>
<!-- =================== TABLE ================ -->
      <div class="scroll-table">
        <table class="table table-shadow table-hover table-responsive">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th width="25%">Nama Guru</th>
              <th width="10%">Nama Mapel</th>
              <th width="5%">Tanggal Lahir</th>
              <th width="5%">Jenis Kelamin</th>
              <th width="10%">No Telepon</th>
              <th width="5%">Level</th>
              <th width="25%" colspan="2"></th>
            </tr>
          </thead>
          <tbody>
            <?php
            include "../php/koneksi.php";
            $query = mysql_query("select * from tm_guru");
            $no = 1;
            while ($field = mysql_fetch_array($query)) {
              ?>
            <tr>
              <form method="post" action="../php/p_input_uh.php">
                <td><?php echo $no ?></td>
                <td><?php echo $field['nama_guru'] ?></td>
                  <input type="hidden" name="nip" maxlength="3" size="2" value=<?php echo $field['nip'] ?>>
                <td><?php echo $field['nama_mapel'] ?></td>
                <td><?php echo $field['tgl_lahir'] ?></td>
                <td><?php echo $field['jk_guru'] ?></td>
                <td><?php echo $field['no_telp'] ?></td>
                <td><?php echo $field['level'] ?></td>
                <td style="text-align: center;">
                  <a href="?nip=<?php echo $field['nip']?>" class=" pointer btn btn-success btn-aksi"><i class="edit-cel fa fa-edit"></i> Edit</a>
                </td>
                <td style="text-align: center;">
                  <a href="../php/d_guru.php?nip=<?php echo $field['nip']?>" class=" pointer btn btn-danger btn-aksi"><i class="edit-cel fa fa-minus-circle"></i> Delete</a>
                </td>
              </form>
            </tr>
              <?php
              $no++;
            }
            ?>
            </tbody>
        </table>
      </div>
    </div>
  </div>

@endsection
