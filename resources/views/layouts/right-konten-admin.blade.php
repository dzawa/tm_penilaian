<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding: 0;">
        <div class="nav-side-menu">
          <div class="brand"></div>
            <div class="menu-list">
              <ul id="menu-content" class="menu-content collapse out">
                  <!-- <li data-toggle="collapse" data-target="#service" class="collapsed">
                    <a href="#"><i class="fa fa-globe fa-lg"></i> Services <span class="arrow"></span></a>
                  </li>
                  <ul class="sub-menu collapse" id="service">
                    <li>New Service 1</li>
                    <li>New Service 2</li>
                    <li>New Service 3</li>
                  </ul> -->
                  <li>
                    <a href="admin_index.php"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                  </li>
                  <li>
                    <a href="admin_guru.php"><i class="fa fa-id-badge" aria-hidden="true"></i> Data Guru</a>
                  </li>
                  <li>
                    <a href="admin_siswa.php"><i class="fa fa-id-card" aria-hidden="true"></i> Data Siswa</a>
                  </li>
                  <li>
                    <a href="admin_jurusan.php"><i class="fa fa-sitemap" aria-hidden="true"></i> Jurusan</a>
                  </li>
                  <li>
                    <a href="admin_kelas.php"><i class="fa fa-users" aria-hidden="true"></i> Kelas</a>
                  </li>
                  <li>
                    <a href="admin_ajaran.php"><i class="fa fa-book" aria-hidden="true"></i> Tahun Ajaran</a>
                  </li>
                  <li>
                    <a href="../php/logout.php"><i class="fa fa-sign-out-alt" aria-hidden="true"></i> Logout</a>
                  </li>
              </ul>
          </div>
        </div>
      </div>
